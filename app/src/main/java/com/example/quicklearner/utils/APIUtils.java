package com.example.quicklearner.utils;

import android.app.Activity;
import android.widget.Toast;

import com.example.quicklearner.dictionary.DictionaryResult;
import com.example.quicklearner.dictionary.parser.WordsApiResponseMessage;
import com.example.quicklearner.dictionary.parser.WordsApiService;
import com.example.quicklearner.exceptions.EmptyApiKeyException;
import com.example.quicklearner.ui.DictionaryResultsRecyclerViewAdapter;
import com.example.quicklearner.ui.LoadingDialog;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIUtils {
    public static void makeRequest(final CompositeDisposable compositeDisposable,
                                   final DictionaryResultsRecyclerViewAdapter adapter,
                                   final Activity contextForLoading,
                                   final String wordToSearch,
                                   final String apiKey) throws EmptyApiKeyException {
        final LoadingDialog dialog = new LoadingDialog(contextForLoading, "Fetching words...");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://wordsapiv1.p.rapidapi.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        if (apiKey.length() == 0) {
            throw new EmptyApiKeyException();
        }
        WordsApiService apiService = retrofit.create(WordsApiService.class);

        dialog.startLoading();
        Single<WordsApiResponseMessage> response = apiService.executeQuery(wordToSearch,
                                                                     apiKey);

        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<WordsApiResponseMessage>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(WordsApiResponseMessage responseMessage) {
                if (responseMessage.hasSucceeded()) {
                    List<DictionaryResult> results = adapter.getValues();
                    results.clear();
                 List<DictionaryResult> queryResults = DictionaryResult.parseResponse(responseMessage);
                 results.addAll(queryResults);
                 adapter.notifyDataSetChanged();
                }
                dialog.stopLoading();
            }

            @Override
            public void onError(Throwable e) {
                dialog.stopLoading();
                if (e instanceof HttpException) {
                    HttpException exception = (HttpException)e;
                    if (exception.code() == 404) {
                        Toast.makeText(contextForLoading, "No results for " + wordToSearch, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(contextForLoading, "An HTTP error while fetching results (code "
                                                                + exception.code() + ")",
                                       Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(contextForLoading, "An unknown error has occurred while fetching the results from the REST API", Toast.LENGTH_LONG).show();
                }
                contextForLoading.finish();
            }
        });

    }
}
