package com.example.quicklearner.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DictionaryResult> ITEMS = new ArrayList<DictionaryResult>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DictionaryResult> ITEM_MAP = new HashMap<String, DictionaryResult>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(DictionaryResult item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.m_word, item);
    }

    private static DictionaryResult createDummyItem(int position) {
        List<String> vals = new ArrayList<String>();
        vals.add("Pastenino");
        return new DictionaryResult(String.valueOf(position),
                                    "Item " + position + "\n\n\n\n\n\n\nCoolstuff\n\n\n\n\n",
                                    vals,
                                    "Verb");
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }
}
