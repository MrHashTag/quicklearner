package com.example.quicklearner.exceptions;

public class AnkiDroidApiNotAvailableException extends  AnkiDroidApiException {
    public AnkiDroidApiNotAvailableException()  {
        super("The AnkiDroid API is not available!");
    }
}
