package com.example.quicklearner.dictionary.parser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WordsApiResultMessage {
    @Expose
    @SerializedName("definition")
    private String definition;
    @Expose
    @SerializedName("partOfSpeech")
    private String partOfSpeech;
    @Expose
    @SerializedName("synonyms")
    private List<String> synonyms;
    @Expose
    @SerializedName("typeOf")
    private List<String> typeOf;
    @Expose
    @SerializedName("hasTypes")
    private List<String> hasTypes;


    public String getDefinition() {
        return definition;
    }

    public String getPartOfSpeech() {
        return partOfSpeech;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

}
