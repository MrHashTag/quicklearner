package com.example.quicklearner.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.quicklearner.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class WordChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_choice);
        final EditText wordToSearchView = findViewById(R.id.tb_wordToSearch);
        final FloatingActionButton searchButton = (FloatingActionButton) findViewById(R.id.b_search);
        final FloatingActionButton settingsButton = (FloatingActionButton) findViewById(R.id.b_settings);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String wordToSearch = wordToSearchView.getText().toString();
                if (wordToSearch.length() == 0) {
                    Toast.makeText(WordChoiceActivity.this, "An empty word has been entered.", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intObj = new Intent(WordChoiceActivity.this, DictionaryResultListActivity.class);
                intObj.putExtra("WORD_TO_SEARCH", wordToSearchView.getText().toString());
                startActivity(intObj);
            }
        });

        settingsButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intObj = new Intent(WordChoiceActivity.this, SettingsActivity.class);
                startActivity(intObj);
            }
        });

    }
}
