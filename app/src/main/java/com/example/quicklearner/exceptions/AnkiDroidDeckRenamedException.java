package com.example.quicklearner.exceptions;

public class AnkiDroidDeckRenamedException extends Exception {
    private String m_previousName;
    private String m_currentName;

    public AnkiDroidDeckRenamedException(final String previousName, final String currentName) {
        super("The AnkiDroid deck has been renamed from " + previousName + " to " + currentName
        + ". Please update the name in the settings.");

        m_previousName = previousName;
        m_currentName = currentName;
    }
}
