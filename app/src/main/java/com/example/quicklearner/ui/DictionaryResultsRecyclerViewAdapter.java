package com.example.quicklearner.ui;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.quicklearner.R;
import com.example.quicklearner.dictionary.DictionaryResult;
import com.google.android.material.chip.Chip;

import java.util.ArrayList;
import java.util.List;

public class DictionaryResultsRecyclerViewAdapter
        extends RecyclerView.Adapter<DictionaryResultsRecyclerViewAdapter.ViewHolder> {

    private final List<DictionaryResult> m_values;
    private final List<DictionaryResult> m_chosenResults;
    private final DictionaryResultListActivity m_parentActivity;
    
    private final Chip.OnCheckedChangeListener mOnCheckedChangeListener = new Chip.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton chip, boolean switched) {
            DictionaryResult item = (DictionaryResult) chip.getTag();

            if (switched) {
                m_chosenResults.add(item);
                Log.i("APP", "Added! " + item.m_word);
            } else {
                m_chosenResults.remove(item);
                Log.i("APP", "Removed! " + item.m_word);
            }
        }
    };

    public DictionaryResultsRecyclerViewAdapter(DictionaryResultListActivity parentActivity, List<DictionaryResult> items) {
        m_parentActivity = parentActivity;
        m_values = items;
        m_chosenResults = new ArrayList<DictionaryResult>();
    }

    public List<DictionaryResult> getValues() {
        return m_values;
    }
    public List<DictionaryResult> getChosenResults() { return m_chosenResults;}

    public void reset() {
        m_values.clear();
        m_chosenResults.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.word_result, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final DictionaryResult result = m_values.get(position);
        holder.m_word.setText(result.getWord() + " (" + result.getPartOfSpeech() + ")");
        holder.m_description.setText("Description: " + result.getDescription());
        holder.m_synonyms.setText(result.getSynonymsString());
        holder.m_included.setOnCheckedChangeListener(mOnCheckedChangeListener);
        holder.m_included.setTag(m_values.get(position));
        holder.m_included.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Nothing
            }
        });
        holder.m_moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new WordFocus(m_parentActivity, result).showWordFocusDialog();
            }
        });


    }

    @Override
    public int getItemCount() {
        return m_values.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView m_word;
        final TextView m_description;
        final TextView m_synonyms;
        final Chip m_included;
        final Button m_moreInfo;

        ViewHolder(View view) {
            super(view);
            m_word = (TextView) view.findViewById(R.id.tv_word);
            m_description = (TextView) view.findViewById(R.id.tv_description);
            m_synonyms = (TextView) view.findViewById(R.id.tv_synonyms);
            m_included = (Chip) view.findViewById(R.id.ch_included);
            m_moreInfo = (Button) view.findViewById(R.id.b_moreInfo);
        }
    }
}