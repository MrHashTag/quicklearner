package com.example.quicklearner.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.quicklearner.R;

public class LoadingDialog {

    private Activity m_activity;
    private AlertDialog m_alertDialog;
    private String m_dialogString;

    public LoadingDialog(Activity parentActivity, String dialog) {
        m_activity = parentActivity;
        m_dialogString = dialog;
    }

    public void startLoading() {
        AlertDialog.Builder builder = new AlertDialog.Builder(m_activity);

        LayoutInflater inflater = m_activity.getLayoutInflater();
        View loadingView = inflater.inflate(R.layout.loading_dialog, null);
        TextView loadingText = loadingView.findViewById(R.id.tv_loading);
        if (m_dialogString != null) {
            loadingText.setText(m_dialogString);
        }
        builder.setView(loadingView);
        builder.setCancelable(false);

        m_alertDialog = builder.create();
        m_alertDialog.show();
    }

    public void stopLoading() {
        m_alertDialog.dismiss();
    }
}
