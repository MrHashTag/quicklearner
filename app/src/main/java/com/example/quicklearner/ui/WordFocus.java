package com.example.quicklearner.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.quicklearner.R;
import com.example.quicklearner.dictionary.DictionaryResult;

public class WordFocus {

    private Activity m_parentActivity;
    private AlertDialog m_alertDialog;
    private DictionaryResult m_dictionaryResult;

    public WordFocus(Activity parentActivity, DictionaryResult dictionaryResult) {
        m_parentActivity = parentActivity;
        m_dictionaryResult = dictionaryResult;
        m_alertDialog = null;
    }

    public void showWordFocusDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(m_parentActivity);

        LayoutInflater inflater = m_parentActivity.getLayoutInflater();
        View wordFocusView = inflater.inflate(R.layout.word_result_focused, null);

        TextView tv_word = wordFocusView.findViewById(R.id.tv_word);
        TextView tv_description = wordFocusView.findViewById(R.id.tv_description);
        TextView tv_synonyms = wordFocusView.findViewById(R.id.tv_synonyms);

        if (tv_word != null) {
            tv_word.setText(String.format("%s (%s)",
                                          m_dictionaryResult.getWord(),
                                          m_dictionaryResult.getPartOfSpeech()));
        }
        if (tv_description != null) {
            tv_description.setText(String.format("Description: %s",
                                                 m_dictionaryResult.getDescription()));
            tv_description.setMovementMethod(new ScrollingMovementMethod());
        }
        if (tv_synonyms != null) {
            tv_synonyms.setText(m_dictionaryResult.getSynonymsString());
            tv_synonyms.setMovementMethod(new ScrollingMovementMethod());
        }

        builder.setView(wordFocusView);
        builder.setCancelable(true);

        m_alertDialog = builder.create();
        m_alertDialog.show();
    }

}
