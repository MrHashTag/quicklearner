package com.example.quicklearner.dictionary;

import com.example.quicklearner.dictionary.parser.WordsApiResponseMessage;
import com.example.quicklearner.dictionary.parser.WordsApiResultMessage;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DictionaryResult {
    public final String m_word;
    public final String m_description ;
    public final List<String> m_synonyms;
    public final String m_partOfSpeech;


    public DictionaryResult(String word,
                            String description,
                            List<String> synonyms,
                            String partOfSpeech) {
        this.m_word = word;
        this.m_description = description;
        this.m_synonyms = synonyms;
        this.m_partOfSpeech = partOfSpeech;
    }

    public String getWord() {
        return m_word;
    }

    public String getDescription() {
        if (m_description == null || m_description.length() == 0) {
            return "Empty";
        }
        return m_description;
    }

    public List<String> getSynonyms() {
        return  m_synonyms;
    }

    public String getSynonymsString() {
        StringBuilder b = new StringBuilder();
        b.append("Synonyms: ");
        if (m_synonyms != null) {
            for (int i = 0; i < m_synonyms.size(); i++) {
                b.append(m_synonyms.get(i));
                if (i < m_synonyms.size() - 1) {
                    b.append(", ");
                }
            }
        }
        return b.toString();
    }

    public String getPartOfSpeech() {
        return m_partOfSpeech;
    }

    // Should be called after the response has been verified
    public static List<DictionaryResult> parseResponse(WordsApiResponseMessage responseMessage) {
        List<DictionaryResult> result = new ArrayList<DictionaryResult>();

        for (WordsApiResultMessage wordResult : responseMessage.getResults()) {
            result.add(parseInnerResponse(responseMessage.getWord(), wordResult));
        }

        return result;
    }

    static private DictionaryResult parseInnerResponse(String word, WordsApiResultMessage responseMessage) {
        return new DictionaryResult(word,
                   responseMessage.getDefinition(),
                   responseMessage.getSynonyms(),
                   responseMessage.getPartOfSpeech());
    }

    public Map<String, String> getDefinitionMap() {
        Map<String, String> result = new HashMap<String, String>();
        result.put("Expression", m_word);
        result.put("Meaning", toString());
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(m_partOfSpeech).append("\n");
        b.append(m_description).append("\n");
        b.append(getSynonymsString());
        return b.toString();
    }
}