package com.example.quicklearner.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quicklearner.R;
import com.example.quicklearner.dictionary.DictionaryResult;
import com.example.quicklearner.exceptions.AnkiDroidApiCallFailedException;
import com.example.quicklearner.exceptions.AnkiDroidApiDeckCreationFailedException;
import com.example.quicklearner.exceptions.AnkiDroidApiNotAvailableException;
import com.example.quicklearner.exceptions.AnkiDroidDeckRenamedException;
import com.example.quicklearner.exceptions.EmptyApiKeyException;
import com.example.quicklearner.utils.APIUtils;
import com.example.quicklearner.utils.AnkiIntegrationHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

/**
 * An activity representing a list of DictionaryResults.
 */
public class DictionaryResultListActivity extends AppCompatActivity {
    private CompositeDisposable m_compositeDisposable;
    private List<DictionaryResult> m_chosenResults;
    private List<DictionaryResult> m_receivedResults;
    AnkiIntegrationHelper m_integrationHelper;

    private static final int AD_PERM_REQUEST = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionaryresult_list);

        m_receivedResults = new ArrayList<DictionaryResult>();
        m_compositeDisposable = new CompositeDisposable();
        try {
            m_integrationHelper = new AnkiIntegrationHelper(this);
        } catch (AnkiDroidApiNotAvailableException e) {
            Toast.makeText(this, "The AnkiDroid API is inaccessible", Toast.LENGTH_LONG).show();
            return;
        }

        final RecyclerView recyclerView = findViewById(R.id.dictionaryresult_list);
        assert recyclerView != null;
        setupRecyclerView(recyclerView);

        Button addToAnkiButton = (Button) findViewById(R.id.b_addToAnki);
        addToAnkiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_integrationHelper.requestPermission(DictionaryResultListActivity.this, AD_PERM_REQUEST);
            }
        });

        Intent searchIntent = getIntent();

        String wordToSearch = (String) searchIntent.getSerializableExtra("WORD_TO_SEARCH");
        if (wordToSearch == null) {
            Toast.makeText(this, "No word to search has been found!", Toast.LENGTH_LONG).show();
            return;
        }

        SharedPreferences decksDb = getSharedPreferences(getString(R.string.settings_shared_preference_name), Context.MODE_PRIVATE);
        String apiKey = decksDb.getString("api_key", "");

        doSearch(wordToSearch, apiKey);

    }

    void doSearch(String wordToSearch, String apiKey) {
        final RecyclerView recyclerView = findViewById(R.id.dictionaryresult_list);
        final DictionaryResultsRecyclerViewAdapter adapter = (DictionaryResultsRecyclerViewAdapter) recyclerView.getAdapter();
        assert adapter != null;
        m_chosenResults = adapter.getChosenResults();

        try {
            APIUtils.makeRequest(m_compositeDisposable,
                                 adapter,
                                 DictionaryResultListActivity.this,
                                 wordToSearch,
                                 apiKey);
        } catch (EmptyApiKeyException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            finish();
        }
    }
    @Override
    protected void onDestroy() {
        if (!m_compositeDisposable.isDisposed()) {
            m_compositeDisposable.dispose();
        }
        super.onDestroy();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        // TODO: Remove DummyContent?
       // m_receivedResults.addAll(DummyContent.ITEMS);
        DictionaryResultsRecyclerViewAdapter adapter = new DictionaryResultsRecyclerViewAdapter(DictionaryResultListActivity.this,
                                                                                                m_receivedResults);
        recyclerView.setAdapter(adapter);
    }

    public void onRequestPermissionsResult (int requestCode, @NonNull String[] permissions,
                                            @NonNull int[] grantResults) {
        if (requestCode==AD_PERM_REQUEST && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            List<Map<String, String>> wordsToAdd = new LinkedList<Map<String, String>>();
            if (m_chosenResults.isEmpty()) {
                Toast.makeText(this, "No results have been chosen!", Toast.LENGTH_LONG).show();
                return;
            }
            for (DictionaryResult result : m_chosenResults) {
                wordsToAdd.add(result.getDefinitionMap());
            }
            try {
                m_integrationHelper.addNewWords(wordsToAdd);
                final RecyclerView recyclerView = findViewById(R.id.dictionaryresult_list);
                setupRecyclerView(recyclerView);
                Toast.makeText(this, "The words have been succesfully added to Anki", Toast.LENGTH_LONG).show();
                this.finish();
            } catch (AnkiDroidApiCallFailedException | AnkiDroidApiNotAvailableException | AnkiDroidApiDeckCreationFailedException | AnkiDroidDeckRenamedException e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "The permission to add words has been denied :(", Toast.LENGTH_LONG).show();
        }
    }
}
