package com.example.quicklearner.exceptions;

public class AnkiDroidApiDeckCreationFailedException extends  AnkiDroidApiException {
    public AnkiDroidApiDeckCreationFailedException() {
        super("Creating an AnkiDroid deck has failed");
    }
}
