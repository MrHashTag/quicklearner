package com.example.quicklearner.exceptions;

public class EmptyApiKeyException extends Exception {
    public EmptyApiKeyException() {
        super("The used API key is empty.");
    }
}
