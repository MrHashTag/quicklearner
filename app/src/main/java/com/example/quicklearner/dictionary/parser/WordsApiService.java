package com.example.quicklearner.dictionary.parser;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WordsApiService {
    @Headers({
            "x-rapidapi-host:wordsapiv1.p.rapidapi.com"
    })
    @GET("words/{word}")
    Single<WordsApiResponseMessage> executeQuery(@Path("word") String word,
                                                 @Header("x-rapidapi-key") String apiKey);
}
