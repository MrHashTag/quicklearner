package com.example.quicklearner.exceptions;

public class AnkiDroidApiException extends Exception {
    public AnkiDroidApiException(String errorMessage) {
        super("AnkiDroidApiException: " + errorMessage);
    }
}
