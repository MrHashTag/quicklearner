package com.example.quicklearner.exceptions;

public class AnkiDroidApiCallFailedException extends AnkiDroidApiException {
    public AnkiDroidApiCallFailedException() {
        super("The AnkiDroid API call has failed");
    }
}