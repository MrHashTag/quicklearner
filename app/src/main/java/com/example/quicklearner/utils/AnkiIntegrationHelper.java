package com.example.quicklearner.utils;

import com.example.quicklearner.R;
import com.example.quicklearner.exceptions.*;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.SparseArray;

import androidx.core.app.ActivityCompat;

import com.example.quicklearner.utils.AnkiDroidConfig;
import com.ichi2.anki.api.AddContentApi;
import com.ichi2.anki.api.NoteInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import static com.ichi2.anki.api.AddContentApi.READ_WRITE_PERMISSION;

public class AnkiIntegrationHelper {
    private Context m_context;
    private AddContentApi m_api;

    private static final String DB_DECK_REF = "com.example.quicklearner.deck";

    public AnkiIntegrationHelper(Context context) throws AnkiDroidApiNotAvailableException {
        m_context = context.getApplicationContext();
        m_api = new AddContentApi(m_context);

        if (!isAnkiDroidApiAvailable()) {
            throw new AnkiDroidApiNotAvailableException();
        }
    }

    //Note: Currently a new word is added into a predefined deck
    // We might want to change this in the future.
    public void addNewWords(final List<Map<String, String>> data) throws AnkiDroidApiCallFailedException,
            AnkiDroidApiNotAvailableException, AnkiDroidApiDeckCreationFailedException, AnkiDroidDeckRenamedException {
        if (!isAnkiDroidApiAvailable()) {
            throw new AnkiDroidApiNotAvailableException();
        }

        SharedPreferences decksDb = m_context.getSharedPreferences(m_context.getString(R.string.settings_shared_preference_name), Context.MODE_PRIVATE);
        String deckName = decksDb.getString("deckname", m_context.getString(R.string.default_deck_name));

        Long deckId = findDeckIdByName(deckName);
        Long modelId = getModelId();

        if (deckId == null) {
            deckId = m_api.addNewDeck(deckName);

            if (deckId == null) {
                throw new AnkiDroidApiDeckCreationFailedException();
            }
            // Only if we succeed in adding a new deck, We update the SharedPreference to be compliant
            // to the configuration of the app as well
            decksDb.edit().putLong(deckName, deckId);
        }

        if (deckId == null || modelId == null) {
            throw new AnkiDroidApiCallFailedException();
        }
        String[] fieldNames = m_api.getFieldList(modelId);
        if (fieldNames == null) {
            throw new AnkiDroidApiCallFailedException();
        }
        // Build list of fields and tags
        LinkedList<String[]> fields = new LinkedList<>();
        LinkedList<Set<String>> tags = new LinkedList<>();
        for (Map<String, String> fieldMap : data) {
            // Build a field map accounting for the fact that the user could have changed the fields in the model
            String[] flds = new String[fieldNames.length];
            for (int i = 0; i < flds.length; i++) {
                // Fill up the fields one-by-one until either all fields are filled or we run out of fields to send
                if (i < AnkiDroidConfig.FIELDS.length) {
                    flds[i] = fieldMap.get(AnkiDroidConfig.FIELDS[i]);
                } else {
                    break;
                }
            }
            tags.add(AnkiDroidConfig.TAGS);
            fields.add(flds);
        }

        removeDuplicates(fields, tags, modelId);
        int added = m_api.addNotes(modelId, deckId, fields, tags);
        if (added == 0) {
            throw new AnkiDroidApiCallFailedException();
        }

    }

    /**
     * Remove the duplicates from a list of note fields and tags
     * @param fields List of fields to remove duplicates from
     * @param tags List of tags to remove duplicates from
     * @param modelId ID of model to search for duplicates on
     */
    private void removeDuplicates(LinkedList<String []> fields, LinkedList<Set<String>> tags, long modelId) {
        // Build a list of the duplicate keys (first fields) and find all notes that have a match with each key
        List<String> keys = new ArrayList<>(fields.size());
        for (String[] f: fields) {
            keys.add(f[0]);
        }
        SparseArray<List<NoteInfo>> duplicateNotes = m_api.findDuplicateNotes(modelId, keys);
        // Do some sanity checks
        if (tags.size() != fields.size()) {
            throw new IllegalStateException("List of tags must be the same length as the list of fields");
        }
        if (duplicateNotes == null || duplicateNotes.size() == 0 || fields.size() == 0 || tags.size() == 0) {
            return;
        }
        if (duplicateNotes.keyAt(duplicateNotes.size() - 1) >= fields.size()) {
            throw new IllegalStateException("The array of duplicates goes outside the bounds of the original lists");
        }
        // Iterate through the fields and tags LinkedLists, removing those that had a duplicate
        ListIterator<String[]> fieldIterator = fields.listIterator();
        ListIterator<Set<String>> tagIterator = tags.listIterator();
        int listIndex = -1;
        for (int i = 0; i < duplicateNotes.size(); i++) {
            int duplicateIndex = duplicateNotes.keyAt(i);
            while (listIndex < duplicateIndex) {
                fieldIterator.next();
                tagIterator.next();
                listIndex++;
            }
            fieldIterator.remove();
            tagIterator.remove();
        }
    }

    public boolean isAnkiDroidApiAvailable() {
        return AddContentApi.getAnkiDroidPackageName(m_context) != null;
    }

    /**
     * Request permission from the user to access the AnkiDroid API (for SDK 23+)
     *
     * @param callbackActivity An Activity which implements onRequestPermissionsResult()
     * @param callbackCode     The callback code to be used in onRequestPermissionsResult()
     */
    public void requestPermission(Activity callbackActivity, int callbackCode) {
        ActivityCompat.requestPermissions(callbackActivity,
                                          new String[]{READ_WRITE_PERMISSION},
                                          callbackCode);
    }

    private Long getDeckId() throws AnkiDroidApiNotAvailableException {
        if (!isAnkiDroidApiAvailable()) {
            throw new AnkiDroidApiNotAvailableException();
        }
        Long deckId = getDeckId(AnkiDroidConfig.s_deckName);
        return deckId;
    }

    /**
     * get model id
     *
     * @return might be null if there was an error
     */
    private Long getModelId() throws AnkiDroidApiNotAvailableException {
        if (!isAnkiDroidApiAvailable()) {
            throw new AnkiDroidApiNotAvailableException();
        }
        Long mid = findModelIdByName(AnkiDroidConfig.MODEL_NAME, AnkiDroidConfig.FIELDS.length);
        if (mid == null) {
            mid = m_api.addNewCustomModel(AnkiDroidConfig.MODEL_NAME,
                                          AnkiDroidConfig.FIELDS,
                                          AnkiDroidConfig.CARD_NAMES,
                                          AnkiDroidConfig.QFMT,
                                          AnkiDroidConfig.AFMT,
                                          AnkiDroidConfig.CSS,
                                          getDeckId(),
                                          null);
        }
        return mid;
    }

    /**
     * Get the ID of the deck which matches the name
     *
     * @param deckName Exact name of deck (note: deck names are unique in Anki)
     * @return the ID of the deck that has given name, or null if no deck was found or API error
     */
    private Long getDeckId(String deckName) throws AnkiDroidApiNotAvailableException {
        if (!isAnkiDroidApiAvailable()) {
            throw new AnkiDroidApiNotAvailableException();
        }
        Map<Long, String> deckList = m_api.getDeckList();
        if (deckList != null) {
            for (Map.Entry<Long, String> entry : deckList.entrySet()) {
                if (entry.getValue().equalsIgnoreCase(deckName)) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    /**
     * Tries to find a deck whose name corresponds with the entered deckName.
     * If the deck has been renamed, throws an exception so the user can decide how to deal
     * with that.
     * @param deckName the name of the deck to find
     * @return the did of the deck in Anki
     */
    private Long findDeckIdByName(String deckName) throws AnkiDroidApiNotAvailableException, AnkiDroidDeckRenamedException {
        // Look for deckName in the deck list
        Long deckId = getDeckId(deckName);

        if (deckId == null) {
            SharedPreferences decksDb = m_context.getSharedPreferences(DB_DECK_REF, Context.MODE_PRIVATE);
            deckId = decksDb.getLong(deckName, -1);
            // If we have a saved deckId from before and the ID is valid, it probably means the deck has
            // been renamed. Throw an indication!
            String newDeckName = m_api.getDeckName(deckId);
            if (deckId != -1 && newDeckName != null) {
               throw new AnkiDroidDeckRenamedException(deckName, newDeckName);
            }
            deckId = null;
        }

        return deckId;
    }

    /**
     * Tries to find the model ID by the entered model name.
     * Doesn't support renaming of the model.
     *
     * @param modelName the name of the model to find
     * @param numFields the minimum number of fields the model is required to have
     * @return the model ID or null if something went wrong
     */
    public Long findModelIdByName(String modelName, int numFields) throws AnkiDroidApiNotAvailableException {
        if (!isAnkiDroidApiAvailable()) {
            throw new AnkiDroidApiNotAvailableException();
        }
        Map<Long, String> modelList = m_api.getModelList(numFields);
        if (modelList != null) {
            for (Map.Entry<Long, String> entry : modelList.entrySet()) {
                if (entry.getValue().equals(modelName)) {
                    return entry.getKey(); // first model wins
                }
            }
        }
        // model no longer exists (by name nor old id), the number of fields was reduced, or API error
        return null;
    }

}
