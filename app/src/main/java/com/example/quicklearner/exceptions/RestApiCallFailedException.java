package com.example.quicklearner.exceptions;

public class RestApiCallFailedException extends Exception {
    public RestApiCallFailedException() {
        super("The REST API call has failed");
    }
}