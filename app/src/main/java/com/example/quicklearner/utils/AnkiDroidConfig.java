package com.example.quicklearner.utils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class AnkiDroidConfig {
    public static String s_deckName = "Random Deck";
    // Name of model which will be created in AnkiDroid
    static final String MODEL_NAME = "QuickLearnerModel";
    // A list of tags to be added to each word
    public static final Set<String> TAGS = new HashSet<>(Collections.singletonList("QuickLearner"));

    static final String CSS = ".card {\n" +
            " font-family: NotoSansJP;\n" +
            " font-size: 24px;\n" +
            " text-align: center;\n" +
            " color: black;\n" +
            " background-color: white;\n" +
            " word-wrap: break-word;\n" +
            "}\n" +
            "@font-face { font-family: \"NotoSansJP\"; src: url('_NotoSansJP-Regular.otf'); }\n" +
            "@font-face { font-family: \"NotoSansJP\"; src: url('_NotoSansJP-Bold.otf'); font-weight: bold; }\n" +
            "\n" +
            ".big { font-size: 48px; }\n" +
            ".small { font-size: 18px;}\n";
    static final String[] FIELDS = {"Expression","Meaning"};

    // Format of the questions
    static final String QFMT1 = "<div class=big>{{Expression}}</div>";
    static final String QFMT2 = "<div class=big>{{Meaning}}</div>";
    public static final String[] QFMT = {QFMT1, QFMT2};

    // Format of the answers
    static final String AFMT1 = QFMT2;
    static final String AFMT2 = QFMT1;
    public static final String[] AFMT = {AFMT1, AFMT2};


    // List of card names that will be used in AnkiDroid (one for each direction of learning)
    static final String[] CARD_NAMES = {"Expression>Meaning", "Meaning>Expression"};

}
