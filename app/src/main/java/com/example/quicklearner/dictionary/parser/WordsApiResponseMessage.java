package com.example.quicklearner.dictionary.parser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WordsApiResponseMessage {
    @Expose
    @SerializedName("success")
    private boolean success;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("word")
    private String word;
    @Expose
    @SerializedName("results")
    private List<WordsApiResultMessage> results;

    public boolean hasSucceeded() {
        return word != null;
    }

    public String getWord() {
        return word;
    }

    public List<WordsApiResultMessage> getResults() {
        return results;
    }

}
